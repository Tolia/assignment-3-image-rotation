//
// Created by 2398768715 on 2023/2/14.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H
#define BMP_SIGNATURE 0x4d42
#define PIXEL_SIZE 24
#define BMP_SIZE 40
#define COMPRESSION 0

#include "file.h"
#include "image.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status {
    READ_SUCCESS = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status {
    WRITE_SUCCESS = 0,
    WRITE_ERROR
};

#pragma pack(pop)

enum read_status bmp_in(FILE *in, struct image *image);

enum write_status bmp_out(FILE *out, struct image *image);

uint32_t cal_img_size(uint32_t width, uint32_t height);

uint32_t cal_file_size(uint32_t img_size);

uint32_t cal_paddding(uint32_t width);

struct bmp_header create_header(struct image *image);

#endif //IMAGE_TRANSFORMER_BMP_H

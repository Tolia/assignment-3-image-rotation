//
// Created by 2398768715 on 2023/3/14.
//

#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H

#include "bmp.h"
#include "image.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

enum state {
    WORKED = 0,
    OPEN_ERROR,
    CLOSE_ERROR
};

void free_file(FILE **file1, FILE **file2);

enum state open_file(FILE **file, char const *path, char const *mode);

enum state close_file(FILE *file);

#endif //IMAGE_TRANSFORMER_FILE_H

//
// Created by 2398768715 on 2023/2/14.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include "bmp.h"
#include "file.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

#pragma pack(push, 1)

struct image {
    uint32_t width;
    uint32_t height;
    struct pixel *data;
};
#pragma pack(pop)

struct image img_create(uint32_t width, uint32_t height);

void img_destroy(struct image img);

struct image img_rotate(struct image const source);

#endif //IMAGE_TRANSFORMER_IMAGE_H

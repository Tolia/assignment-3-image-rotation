//
// Created by 2398768715 on 2023/2/14.
//

#include "bmp.h"
#include "file.h"
#include "image.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

enum {
    BMP_PIXEL_SIZE = sizeof(struct pixel),
    BMP_HEADER_SIZE = sizeof(struct bmp_header)
};

uint32_t cal_paddding(uint32_t width) {
    return (4 - (width * 3) % 4) % 4;

}

struct bmp_header create_header(struct image *image) {
    const uint32_t image_size = cal_img_size(image->width, image->height);
    const uint32_t file_size = cal_file_size(image_size);

    return (struct bmp_header) {
            .bfType = BMP_SIGNATURE,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bOffBits = BMP_HEADER_SIZE,
            .biSize = BMP_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = PIXEL_SIZE,
            .biCompression = COMPRESSION,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

uint32_t cal_img_size(uint32_t width, uint32_t height) {
    return (width + cal_paddding(width)) * BMP_PIXEL_SIZE * height;
}

uint32_t cal_file_size(uint32_t img_size) {
    return (img_size + BMP_HEADER_SIZE);
}

enum read_status bmp_in(FILE *in, struct image *image) {
    struct bmp_header header;

    if (fread(&header, BMP_HEADER_SIZE, 1, in) != 1) {
        return READ_ERROR;
    }

    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != PIXEL_SIZE) {
        return READ_INVALID_BITS;
    }

    if (header.biSize <= 0) {
        return READ_INVALID_HEADER;
    }

    if (header.biWidth <= 0 || header.biHeight <= 0) {
        return READ_INVALID_HEADER;
    }

    *image = img_create(header.biWidth, header.biHeight);

    uint32_t padding = cal_paddding((uint32_t) image->width);

    for (uint64_t i = 0; i < image->height; i++) {
        void *pointerStart = image->data + image->width * i;

        if (fread(pointerStart, BMP_PIXEL_SIZE, image->width, in) != image->width) {
            img_destroy(*image);

            return READ_ERROR;
        }

        if (fseek(in, padding, SEEK_CUR)) {
            img_destroy(*image);

            return READ_ERROR;
        }
    }

    return READ_SUCCESS;
}

enum write_status bmp_out(FILE *out, struct image *image) {
    struct bmp_header out_header;

    uint32_t padding = cal_paddding((uint32_t) image->width);

    out_header = create_header(image);

    if (fwrite(&out_header, BMP_HEADER_SIZE, 1, out) == 0) {
        return WRITE_ERROR;
    }

    for (uint64_t i = 0; i < image->height; i++) {
        void *pointerStart = (image->data + image->width * i);

        if (fwrite(pointerStart, BMP_PIXEL_SIZE, image->width, out) == 0) {
            img_destroy(*image);

            return WRITE_ERROR;
        }
        uint8_t garb[3] = {0};

        if ((padding != 0) && (!fwrite(&garb, padding, 1, out))) {
            img_destroy(*image);

            return WRITE_ERROR;
        }
    }

    return WRITE_SUCCESS;
}

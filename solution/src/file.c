//
// Created by 2398768715 on 2023/2/14.
//
#include "bmp.h"
#include "file.h"
#include "image.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

enum state open_file(FILE **file, char const *path, char const *mode) {
    *file = fopen(path, mode);

    return *file == NULL ? OPEN_ERROR : WORKED;
}
void free_file(FILE **file1, FILE **file2) {
    free(file1);
    free(file2);
}

enum state close_file(FILE *file) {
    if (fclose(file) == 0) {
        return WORKED;
    } else {
        return CLOSE_ERROR;
    }
}
